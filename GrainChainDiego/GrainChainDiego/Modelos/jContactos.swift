//
//  jContactos.swift
//  GrainChainDiego
//
//  Created by Diego Miranda [GDL] on 3/20/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import UIKit
import Foundation

class jContactos: NSObject {
    var sNombre:String=""
    var sApellido:String=""
    var sNumero:String=""
    var sEdad:String=""
    var dImgContacto:Data?
    var iId=0
}
