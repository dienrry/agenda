//
//  PerfilHelper.swift
//  GrainChainDiego
//
//  Created by Diego Miranda [GDL] on 3/21/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension PerfilViewController{
    
    func configPerfil(){
        lblNombre.text=UserDefaults.standard.string(forKey: "Usuario")
        lblApellido.text=UserDefaults.standard.string(forKey: "Apellido")
        lblEmail.text=UserDefaults.standard.string(forKey: "Email")
        lblDomicilio.text=UserDefaults.standard.string(forKey: "Direccion")
    }
    
    func BorrarCoreData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contactos")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(fetchRequest)
            for object in result {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
        } catch _ {
            print("Error Detele all data")
        }
    }
    
    func cerrarSesion(){
        UserDefaults.standard.set(nil, forKey: "Usuario")
        UserDefaults.standard.set(nil, forKey: "Apellido")
        UserDefaults.standard.set(nil, forKey: "Email")
        UserDefaults.standard.set(nil, forKey: "Direccion")
        UserDefaults.standard.set(0, forKey: "iIdContacto")
        performSegue(withIdentifier: "CerrarSesionSegue", sender: nil)
        
    }
    
}
