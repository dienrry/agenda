//
//  LoginHelper.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension LoginViewController{
    
    //MARK: - Inicializadores
    
    // Configuración de la vista
    func configLogin(){
        
        //Inicilización textfields
        txtUsuario.delegate=self
        txtPassword.delegate=self
        
        // Personalizamos los campos txtUsuario y txtPassword
        let paddingForFirst = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.txtUsuario.frame.size.height))
        txtUsuario.leftView = paddingForFirst
        txtUsuario.leftViewMode = UITextField.ViewMode .always
        txtUsuario.attributedPlaceholder = NSAttributedString(string: "Usuario", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        let paddingForSecond = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.txtPassword.frame.size.height))
        txtPassword.leftView = paddingForSecond
        txtPassword.leftViewMode = UITextField.ViewMode .always
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])

    }
    
    //MARK: - Teclado Configuración
    
    // Este método se activa cuando se toca en cualquier parte de la pantalla (se utiliza para ocultar el teclado)
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Este método oculta el teclado al dar click en el botón Enter
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Funciones
    
    func IniciarSesion(sUsuario:String,sPassword:String){
        if sUsuario=="" || sPassword==""{
            
            self.performSegue(withIdentifier:"LoginSegue", sender: self)
            oAlerta.mostrarAlerta("Necesitas ingresar tu usuario y contraseña", title: "Aviso")
        }else{
            
            let oJSON:JSON? = oWebService.Login(sAcceso: sUsuario, sContrasena: sPassword)
            if oJSON?["statusCode"].intValue==200{
                
                UserDefaults.standard.set(oJSON?["body"]["auth"]["user"]["name"].stringValue, forKey: "Usuario")
                UserDefaults.standard.set(oJSON?["body"]["auth"]["user"]["lastname"].stringValue, forKey: "Apellido")
                UserDefaults.standard.set(oJSON?["body"]["auth"]["user"]["email"].stringValue, forKey: "Email")
                UserDefaults.standard.set(oJSON?["body"]["auth"]["user"]["address"].stringValue, forKey: "Direccion")
                
                AgregarContactosPreestablecidos()
                self.performSegue(withIdentifier:"LoginSegue", sender: self)
                
            }else{
                oAlerta.mostrarAlerta("Usuario y/o Contraseña incorrectos", title: "Alerta")
            }
        }//UsuarioIncorrecto
    }//IniciarSesion
    
    func AgregarContactosPreestablecidos(){
        
        let arrayNombre:NSMutableArray = ["Diego","Enrique","Regina","Marcus","Juan","José","Pedro","Luis","Mael","Elías"]
        let arrayApellido:NSMutableArray = ["Miranda","Vázquez","Sánchez","Pérez","López","Campos","González","Carmona","Hernández","Román"]
        let arrayNumero:NSMutableArray = ["3314106835","3312141516","33161719520","3383940184","3327930473","3383723048","3308171926","3388993344","3317789384","331809348"]
        let arrayEdad:NSMutableArray = ["26","22","21","16","10","30","29","25","18","19"]
        let arraybImgContacto:NSMutableArray = [UIImage(named: "eje1")!.pngData()!,UIImage(named: "eje2")!.pngData()!,UIImage(named: "eje3")!.pngData()!,UIImage(named: "eje4")!.pngData()!,UIImage(named: "eje5")!.pngData()!,UIImage(named: "eje6")!.pngData()!,UIImage(named: "eje7")!.pngData()!,UIImage(named: "eje8")!.pngData()!,UIImage(named: "eje9")!.pngData()!,UIImage(named: "eje10")!.pngData()!]
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "Contactos", in: managedContext)!
            
        for i in 0...9{
            let contacto = NSManagedObject(entity: userEntity, insertInto: managedContext)
            contacto.setValue(arrayNombre[i],forKey: "sNombre")
            contacto.setValue(arrayApellido[i], forKey: "sApellido")
            contacto.setValue(arrayNumero[i], forKey: "sNumero")
            contacto.setValue(arrayEdad[i],forKey: "sEdad")
            contacto.setValue(arraybImgContacto[i], forKey: "imgContacto")
            UserDefaults.standard.set((i+1), forKey: "iIdContacto")
            contacto.setValue(UserDefaults.standard.integer(forKey: "iIdContacto"), forKey: "iIdContacto")
        }
        do{
            try managedContext.save()
        }catch _ as NSError{
            print("erroor")
        }
        
    }
    

}//LoginViewController
