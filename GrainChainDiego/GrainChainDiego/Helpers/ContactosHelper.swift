//
//  ContactosHelper.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension ContactosViewController{
    
    //Dara formato a la vista
    func configContactos(){
        
        lblTitulo.text=UserDefaults.standard.string(forKey: "Usuario")
        ConstHViewSearchBar.constant=0
        
        self.lvwContactos.register(UINib(nibName: "ContactosTableViewCell", bundle: nil), forCellReuseIdentifier: "Contactos")
        
        //Se asignan los delegados
        self.lvwContactos.delegate = self
        self.lvwContactos.dataSource = self
        self.SearchBarContactos.delegate=self
        
        //Oculta las celdas vacías
        self.lvwContactos.tableFooterView = UIView(frame:CGRect(x:0,y:0,width:0,height:0))

    }
    
    func MostrarBarraBusqueda(){
        if bBuscar==false{
            self.ConstHViewSearchBar.constant=60
            self.bBuscar=true
        }else{
            self.ConstHViewSearchBar.constant=0
            self.bBuscar=false
        }
    }//MostrarBarraBusqueda
    
    func obtenercontactos(){
        listContactos=[]
        listAgendaFiltro=[]
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contactos")
        let sortDescriptor = NSSortDescriptor(key: "sNombre", ascending: true)
        let sortDescriptors =  [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        do{
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
              let oContactos: jContactos = jContactos()
                oContactos.sNombre=(data.value(forKey:"sNombre")as! String)
                oContactos.sApellido=(data.value(forKey: "sApellido")as! String)
                oContactos.sNumero=(data.value(forKey: "sNumero")as! String)
                oContactos.sEdad=(data.value(forKey: "sEdad")as! String)
                oContactos.dImgContacto=(data.value(forKey: "imgContacto")as! Data)
                oContactos.iId=(data.value(forKey: "iIdContacto")as! Int)
                
                listContactos.add(oContactos)
            }
            print(listContactos.count)
            lvwContactos.reloadData()
        }
        catch{
            print("Fallo")
        }
    }//obtenercontactos
    
    func EliminarContacto(Id:Int){
        print(Id)
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contactos")
        let Idstring:String = String(Id)
        let predicate = NSPredicate(format:"iIdContacto = %@",Idstring)
        fetchRequest.predicate = predicate
        let result = try? context.fetch(fetchRequest)
        let resultData = result as! [NSManagedObject]
        for object in resultData {
            context.delete(object)
        }
        
        do {
            try context.save()
            oAlerta.mostrarAlerta("Contacto eliminado", title: "")
            appDelegate.bEditar=false
            if bBuscando==true{
                bBuscando=false
                SearchBarContactos.text=""
            }
            obtenercontactos()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }//EliminarContacto
    

}

