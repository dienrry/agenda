//
//  AgregarHelper.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AVFoundation

extension AgregarViewController{
    
    //Dara formato a la vista
    func configAgregar(){
        
        lblTitulo.text=UserDefaults.standard.string(forKey: "Usuario")

        //Formato a los textfields
        txtNombre.attributedPlaceholder = NSAttributedString(string: "Nombre", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtNombre.addBottomBorder()
        
         txtApellido.attributedPlaceholder = NSAttributedString(string: "Apellidos", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtApellido.addBottomBorder()
        
         txtTelefono.attributedPlaceholder = NSAttributedString(string: "Teléfono", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtTelefono.addBottomBorder()
        
         txtEdad.attributedPlaceholder = NSAttributedString(string: "Edad", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtEdad.addBottomBorder()
        
        //Se colocan las imagenes en forma de circulo
        imgAgregar.layer.cornerRadius = 10
        imgAgregar.layer.masksToBounds=true
        imgAgregar.clipsToBounds = true
        
        //Se agrega la acción para cuando se de click en la imagen
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(AgregarViewController.imageTapped(_:)))
        imgAgregar.addGestureRecognizer(tapGestureRecognizer)
        imgAgregar.isUserInteractionEnabled = true
        
    }//config

    
    //MARK: - Teclado Configuración
    
    // Este método se activa cuando se toca en cualquier parte de la pantalla (se utiliza para ocultar el teclado)
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Este método oculta el teclado al dar click en el botón Enter
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: - Camara

    //Este método se activa al dar click en la imagen.
    @objc func imageTapped(_ img: AnyObject){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Por favor, selecciona el origen de la imágen", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Tomar fotografía", style: .default, handler: { (action: UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.bImgAgregada=true
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                print("La cámara no está disponible.")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Biblioteca fotográfica", style: .default, handler: { (action: UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.bImgAgregada=true
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        guard let image = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage else {
                return
        }
        imgAgregar.image = image
        imageData = image.pngData()
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
   
    //MARK: - Funciones
    
    func ValidarCampos() {
        if txtNombre.text=="" || txtApellido.text=="" || txtTelefono.text=="" || txtEdad.text==""{
            oAlerta.mostrarAlerta("Todos los campos son obligatorios de completar", title: "Aviso")
        }else{
            guardarcontacto()
            oAlerta.mostrarAlerta("Contacto agregado", title: "Aviso")
            performSegue(withIdentifier: "segueContactoAgregado", sender: nil)
        }
    }
    
    //Guardar Contacto
    func guardarcontacto(){
        var aux=UserDefaults.standard.integer(forKey: "iIdContacto")
        aux=aux+1
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "Contactos", in: managedContext)!
        let contacto = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        contacto.setValue(txtNombre.text, forKey: "sNombre")
        contacto.setValue(txtApellido.text, forKey: "sApellido")
        contacto.setValue(txtTelefono.text, forKey: "sNumero")
        contacto.setValue(txtEdad.text, forKey: "sEdad")
        if bImgAgregada{
            contacto.setValue(imageData, forKey: "imgContacto")
        }else{
            contacto.setValue(UIImage(named:"UsuarioFill")!.pngData()!, forKey: "imgContacto")
        }
        
        contacto.setValue(aux, forKey: "iIdContacto")
        
        do{
            try managedContext.save()
            UserDefaults.standard.setValue(aux, forKey: "iIdContacto")
           
        }catch _ as NSError{
            
            print("erroor")
        }

    }
    
   
    
    
}//AgregarViewController


extension UITextField {

    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
        
    }
}
