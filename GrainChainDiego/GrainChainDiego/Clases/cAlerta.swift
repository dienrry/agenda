//
//  cAlerta.swift
//  RegistroGastos
//
//  Created by Admin on 24/11/15.
//  Copyright © 2015 CompuSoluciones. All rights reserved.
//

import Foundation
import UIKit

class cAlerta: NSObject {
    
    func mostrarAlerta(_ msg: String, title: String) {
        let myAlert = UIAlertView()
        myAlert.title = title
        myAlert.message = msg
        myAlert.addButton(withTitle: "OK")
        myAlert.delegate = self
        myAlert.show()
        
    }
    
    func mostrarAlerta(_ msg: String, title: String, tag: Int, btn1: String) {
        let myAlert = UIAlertView()
        myAlert.title = title
        myAlert.message = msg
        myAlert.addButton(withTitle: btn1)
        myAlert.delegate = self
        myAlert.show()
    }
    
}
