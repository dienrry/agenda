//
//  ContactosViewController.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class ContactosViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate{

    
    //MARK: - variables
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var ConstHViewSearchBar: NSLayoutConstraint!
    @IBOutlet weak var SearchBarContactos: UISearchBar!
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var lvwContactos: UITableView!
    
    
    var bBuscar=false
    var bBuscando = false
    
    var listContactos:NSMutableArray = []
    var listAgendaFiltro:NSMutableArray = []
    
    var oContactos:jContactos=jContactos()
    
    //Inicializador de clases
    let oAlerta = cAlerta()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    //MARK: - Flujo
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configContactos()
        obtenercontactos()
    }
    
    
    //MARK: - Tabla
 
    
    //Este método conserva la altura de la celda colocada desde la celda personalizada
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    //Este método de vuelve el número de secciones
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Este método devuelve el número de filas de la toverride abla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bBuscando == true{
            return listAgendaFiltro.count
        }else{
            return listContactos.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contactos", for: indexPath) as! ContactosTableViewCell
        var oContactos: jContactos!
        if bBuscando==true{
            oContactos=listAgendaFiltro.object(at: indexPath.row) as? jContactos
        }else{
            oContactos=listContactos.object(at: indexPath.row) as? jContactos
        }
        
        if appDelegate.bEditar==false{
            cell.btnEliminar.isHidden=true
            cell.btnEliminar.isEnabled=false
        }else{
            cell.btnEliminar.isHidden=false
            cell.btnEliminar.isEnabled=true
        }

        cell.lblNombre.text = oContactos.sNombre
        cell.lblApellido.text=oContactos.sApellido
        cell.lblEdad.text=(oContactos.sEdad+" Años")
        cell.lblTelefono.text=oContactos.sNumero
        cell.imgContacto.image=UIImage(data: oContactos.dImgContacto!)
        
        cell.btnEliminar.tag=oContactos.iId
        cell.btnEliminar.addTarget(self, action: #selector(BotonEliminar(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    //MARK: - Botones
    
    @IBAction func btnBuscar(_ sender: Any) {
       MostrarBarraBusqueda()
    }

    @IBAction func btnEditar(_ sender: Any) {
        if appDelegate.bEditar==true{
            appDelegate.bEditar=false
        }else{
            appDelegate.bEditar=true
        }
        lvwContactos.reloadData()
    }
    
    // Este método identifica cuando se comienza a escribir y filtra los resultados
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        if SearchBarContactos.text != "" {
            bBuscando = true
        } else {
            bBuscando = false
        }
        
        //No reconoce Nombre como elemento del objeto jContactos :(
        // Por ello truena el searchbar pero desconozo el fallo
        
        /*
        let Predicate = NSPredicate(format: "(Nombre contains [cd] %@)", searchText)
        listAgendaFiltro = ((self.listContactos as NSArray).filtered(using: Predicate) as NSArray).mutableCopy() as! NSMutableArray
        self.lvwContactos.reloadData()
         */

        // Funciona esta manera de filtrar
        listAgendaFiltro = ((listContactos.filter() {
            let type = ($0 as! jContactos)
            let nombre = type.sNombre.folding(options: .diacriticInsensitive, locale: .current)
            let buscador = searchText.folding(options: .diacriticInsensitive, locale: .current)
            return (nombre.lowercased()).contains(buscador.lowercased())
        })as NSArray).mutableCopy() as! NSMutableArray
        
        self.lvwContactos.reloadData()
        
    }
    
    @objc func BotonEliminar(sender:UIButton) {
        let alertController = UIAlertController(title: "Alerta", message: "¿Estás seguro que deseas eliminar el contacto de la agenda?", preferredStyle: .alert)
        let AceptarEliminar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            print(sender.tag)
            self.EliminarContacto(Id:sender.tag)
        }
        alertController.addAction(AceptarEliminar)
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in }
        alertController.addAction(cancelarAction)
        self.present(alertController, animated: true) {}
    }
   
    
}

