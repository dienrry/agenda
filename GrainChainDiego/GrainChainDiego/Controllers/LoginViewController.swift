//
//  LoginViewController.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - Variables
    
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    //Inicializador de clases
    let oAlerta = cAlerta()
    //let oConexion = cConexion()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let oWebService = cWebServices()
    var imageData: Data?
    
    
    //MARK: Flujo de la vista
    override func viewDidLoad() {
        super.viewDidLoad()
        //Da diseño a la vista
        configLogin()
        
    }
    
    //MARK: Botones
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
        IniciarSesion(sUsuario: txtUsuario.text!, sPassword: txtPassword.text!)

    }//btnIniciarSesion

}
