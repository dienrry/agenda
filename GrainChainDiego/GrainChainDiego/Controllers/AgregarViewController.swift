//
//  AgregarViewController.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import CoreData

class AgregarViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate{
    
    //MARK: - Variables
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellido: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtEdad: UITextField!
    @IBOutlet weak var imgAgregar: UIImageView!
    
    var imageData: Data?
    var bImgAgregada=false
    
    //Inicializador de clases
    let oAlerta = cAlerta()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    //MARK: - Flujo
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configAgregar()
    }
    
    
    //MARK: - Funciones
    @IBAction func btnGuardar(_ sender: Any) {
        ValidarCampos()
    }
    

}
