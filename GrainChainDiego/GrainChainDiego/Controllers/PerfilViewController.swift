//
//  PerfilViewController.swift
//  GrainChainDiego
//
//  Created by Diego Miranda [GDL] on 3/21/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import Foundation
import UIKit

class PerfilViewController: UIViewController {
    
    
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblApellido: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDomicilio: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configPerfil()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Botones
 
    @IBAction func btnCerrarSesion(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alerta", message: "¿Estás seguro que deseas cerrar la sesión?. Se eliminaran los datos almacenados", preferredStyle: .alert)
        let AceptarEliminar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.BorrarCoreData()
            self.cerrarSesion()
        }
        alertController.addAction(AceptarEliminar)
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in }
        alertController.addAction(cancelarAction)
        self.present(alertController, animated: true) {}
        
    }
    

}
