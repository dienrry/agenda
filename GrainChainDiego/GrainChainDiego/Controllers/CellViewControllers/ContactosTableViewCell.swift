//
//  ContactosTableViewCell.swift
//  GrainChainDiego
//
//  Created by Diego Miranda on 3/19/19.
//  Copyright © 2019 Diego Miranda. All rights reserved.
//

import UIKit
import Foundation

class ContactosTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblApellido: UILabel!
    @IBOutlet weak var lblEdad: UILabel!
    @IBOutlet weak var lblTelefono: UILabel!
    @IBOutlet weak var imgContacto: UIImageView!
    @IBOutlet weak var btnEliminar: UIButton!
    var Contactos:ContactosViewController=ContactosViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnEliminarAction(_ sender: Any) {
//        let alertController = UIAlertController(title: "Si cancelas la reservación se liberará tú lugar para quien reservó después de ti", message: nil, preferredStyle: .alert)
//        let reintentarAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
//
//        }
//        alertController.addAction(reintentarAction)
//        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in }
//        alertController.addAction(cancelarAction)
//        self.present(alertController, animated: true) {}
  
    }
    
}
